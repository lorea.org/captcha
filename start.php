<?php
/**
 * Elgg captcha plugin
 * 
 * @package ElggCaptcha
 */


register_elgg_event_handler('init','system','captcha_init');

function captcha_init() {

	// Register page handler for captcha functionality
	elgg_register_page_handler('captcha', 'captcha_page_handler');

	// Extend CSS
	elgg_extend_view('css', 'captcha/css');

	// Number of background images
	elgg_set_config('captcha_num_bg', 5);

	// Default length
	elgg_set_config('captcha_length', 5);

	elgg_register_plugin_hook_handler('register', 'user', 'captcha_verify_action_hook', 0);
	elgg_register_plugin_hook_handler('action', 'user/requestnewpassword', 'captcha_verify_action_hook');
}

function captcha_page_handler($page) {

	if (isset($page[0])) {
		set_input('captcha_token', $page[0]);
	}

	include(elgg_get_plugins_path() . "captcha/captcha.php");
}

/**
 * Generate a token to act as a seed value for the captcha algorithm.
 */
function captcha_generate_token() {
	return md5(generate_action_token(time()).rand()); // Use action token plus some random for uniqueness
}

/**
 * Generate a captcha based on the given seed value and length.
 *
 * @param string $seed_token
 * @return string
 */
function captcha_generate_captcha($seed_token) {
	/*
	 * We generate a token out of the random seed value + some session data, 
	 * this means that solving via pr0n site or indian cube farm becomes
	 * significantly more tricky (we hope).
	 * 
	 * We also add the site secret, which is unavailable to the client and so should
	 * make it very very hard to guess values before hand.
	 * 
	 */

	switch(rand(0, 1)) {
		case 0:
			// Plus
			$n1 = rand(1, 9);
			$n2 = rand(1, 9);
			$op = "+";
			$sol = $n1 + $n2;
			break;
		case 1:
			// Minus
			$n1 = rand(10, 20);
			$n2 = rand(1, 9);
			$op = "-";
			$sol = $n1 - $n2;
			break;
	}

	if (!isset($_SESSION['captcha_solution'])) {
		$_SESSION['captcha_solution'] = array();
	}
	$_SESSION['captcha_solution'][$seed_token] = $sol;

	return "$n1 $op $n2 = ";
}

/**
 * Verify a captcha based on the input value entered by the user and the seed token passed.
 *
 * @param string $input_value
 * @param string $seed_token
 * @return bool
 */
function captcha_verify_captcha($input_value, $seed_token) {
	if ($input_value && $input_value == $_SESSION['captcha_solution'][$seed_token]) {
		unset($_SESSION['captcha_solution'][$seed_token]);
		return true;
	}
	return false;
}

/**
 * Listen to the action plugin hook and check the captcha.
 *
 * @param unknown_type $hook
 * @param unknown_type $entity_type
 * @param unknown_type $returnvalue
 * @param unknown_type $params
 */
function captcha_verify_action_hook($hook, $entity_type, $returnvalue, $params) {
	$token = get_input('captcha_token');
	$input = get_input('captcha_input');

	if (($token) && (captcha_verify_captcha($input, $token))) {
		return true;
	}

	register_error(elgg_echo('captcha:math:captchafail'));

	return false;
}
