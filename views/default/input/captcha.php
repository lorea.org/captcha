<?php
/**
 * Elgg captcha plugin captcha hook view override.
 * 
 * @package ElggCaptcha
 */

// Generate a token which is then passed into the captcha algorithm for verification
$token = captcha_generate_token();
?>
<div>
	<label><?php echo elgg_echo('captcha:math:entercaptcha'); ?></label>
	<br />
	<?php
		echo elgg_view('input/hidden', array('name' => "captcha_token", 'value' => $token));
		echo elgg_view('output/img', array('src'=> elgg_get_site_url() . "captcha/$token", 'class' => 'captcha-input-image'));
		echo elgg_view('input/text', array('name' => 'captcha_input', 'class' => 'captcha-input-text'));
	?>
</div>
