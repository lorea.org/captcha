<?php
/**
 * Elgg captcha language pack.
 * 
 * @package ElggCaptcha
 */

$catalan = array( 
	'captcha:entercaptcha' => 'Introdueix el text de la imatge',
	'captcha:captchafail' => 'Perdona, el text que has introduït no correspon amb el text de la imatge.',
	'captcha:math:entercaptcha' => 'Relalitza la següent operació matemàtica',
	'captcha:math:captchafail' => 'Perdona, el resultat que has introduït no correspon amb la operació de la imatge.',	
);

add_translation("ca", $catalan);
