<?php
/**
 * Elgg captcha language pack.
 * 
 * @package ElggCaptcha
 */

$spanish = array( 
	'captcha:entercaptcha' => 'Introduce el texto de la imagen',
	'captcha:captchafail' => 'Perdona, el texto que has introducido no corresponde con el texto de la imagen.',
	'captcha:entercaptcha' => 'Calcula la siguiente operación matemática',
	'captcha:captchafail' => 'Perdona, el resultado que has introducido no corresponde con la operación de la imagen.',
);

add_translation("es", $spanish);
