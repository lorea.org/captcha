<?php
/**
 * Elgg captcha language pack.
 * 
 * @package ElggCaptcha
 */

$english = array( 
	'captcha:entercaptcha' => 'Enter text from image',
	'captcha:captchafail' => 'Sorry, the text that you entered didn\'t match the text in the image.',
	'captcha:math:entercaptcha' => 'Calculate the following matemathic operation',
	'captcha:math:captchafail' => 'Sorry, the result that you entered didn\'t match the operation in the image.',
);

add_translation("en", $english);
